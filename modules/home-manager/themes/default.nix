# Very incomplete, much inspiration (and 'module collecting') from https://github.com/danth/stylix
{...}:{
  imports = [
    ./fonts/default.nix
    ./wallpaper/default.nix
  ];
}
