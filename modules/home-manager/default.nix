{
  xdg = import ./xdg;
  audio = import ./audio;
  shell = import ./shell;
  wayland = import ./wayland;
  themes = import ./themes;
  programs = import ./programs;
  editor = import ./editor;
}
