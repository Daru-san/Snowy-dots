{...}:{
  imports = [
    ./git/default.nix
    ./mpv/default.nix
    ./btop/default.nix
    ./ranger/default.nix
    ./connect/default.nix
    ./translate-shell/default.nix
    ./bat/default.nix
  ];
}
