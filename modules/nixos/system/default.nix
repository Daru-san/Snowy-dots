{...}:{
  imports = [
    ./boot/default.nix
    ./devices/default.nix
    ./network/default.nix
    ./security/default.nix
    ./intel-gpu/default.nix
    ./plymouth/default.nix
    ./audio/default.nix
  ];
}
